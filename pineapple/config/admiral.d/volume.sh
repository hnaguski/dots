#!/bin/bash
# a block that displays volume information

# grab volume of one channel
vol=$(pulsemixer --get-volume | awk '{print $1}')

# grab mute status
mute=$(pulsemixer --get-mute)

# Check how high the volume is and assign an icon

if [ $vol -ge 0 ] && [ $vol -le 33 ]; then
	{
		icon="奄"
	}
elif [ $vol -gt 33 ] && [ $vol -le 66 ]; then
	{
		icon="奔"
	}
elif [ $vol -gt 66 ]; then
	{
		icon="墳"
	}
fi

# check for mute and generate output
if [ $mute == 0 ]; then
	{
	output="${icon}: ${vol}% │ "
	}
else
	{
    output="婢: ${vol}% │ "
	}
fi

# send the output with mouse wheel support (lemonbar prints commands to stdout)
echo %{A4:pulsemixer --change-volume +2:}%{A5:pulsemixer --change-volume -2:}%{A:pulsemixer --toggle-mute:} "$output" %{A}%{A}%{A}

#!/bin/bash
# Script to (re)launch lemonbar, run this script in your wm's config file when you want to start the bar
# Based off polybar's launch.sh

# Get colors from wal, this script uses 'background' and 'forground'
. ~/.cache/wal/colors.sh

# Terminate admiral and lemonbar
killall -q lemonbar admiral

# Wait until the processes have been shut down
while pgrep -u $UID -x admiral && pgrep -u $UID -x lemonbar >/dev/null; do sleep 1; done

# Launch admiral and lemonbar with colors from wal
admiral | lemonbar -b -f "SauceCodePro Nerd Font" -B $background -F $foreground | bash

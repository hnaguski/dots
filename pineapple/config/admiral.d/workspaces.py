#!/usr/bin/env python3

import i3ipc

# Create an object that is a connection to send commands and subscribe to events by i3
i3 = i3ipc.Connection()

# Define a callback when you switch workspaces
def on_workspace_focus(self, e):
    workspace_string = ''
    workspaces = i3.get_workspaces()

    # Make a list of active workspaces
    ws_nums = [int(n['num']) for n in workspaces]

    # Get which workspace is currently focused (starts at 0)
    focused = [n['visible'] for n in workspaces].index(True)
    focused = ws_nums[focused] - 1

    # get a list of urgent workspaces
    urg = [n for n, _ in enumerate(workspaces) if workspaces[n]['urgent'] == True]
    for n in range(len(urg)):
        urg[n] = ws_nums[urg[n]] - 1

    for n in range(10):
        if focused == n:
            workspace_string += '%{A:i3-msg workspace number ' + str(n+1) + ':}' + '%{F#FFFFFF}' + str(n+1) + '%{F-}%{A}' + ' '
        elif n in urg:
            workspace_string += '%{A:i3-msg workspace number ' + str(n+1) + ':}' + '%{F#FF0000}' + str(n+1) + '%{F-}%{A}' + ' '
        elif n+1 in ws_nums:
            workspace_string += '%{A:i3-msg workspace number ' + str(n+1) + ':}' + '%{F#555555}' + str(n+1) + '%{F-}%{A}' + ' '

    if focused == 9:
        print('%{A4:i3-msg workspace prev:}',workspace_string,'%{A}',flush=True)
    elif focused == 0:     
        print('%{A5:i3-msg workspace next:}',workspace_string,'%{A}',flush=True)
    else:
        print('%{A4:i3-msg workspace prev:}%{A5:i3-msg workspace next:}',workspace_string,'%{A}%{A}',flush=True)


workspace_string = ''
workspaces = i3.get_workspaces()

# Make a list of active workspaces
ws_nums = [int(n['num']) for n in workspaces]

# Get which workspace is currently focused (starts at 0)
focused = [n['visible'] for n in workspaces].index(True)
focused = ws_nums[focused] - 1

# get a list of urgent workspaces
urg = [n for n, _ in enumerate(workspaces) if workspaces[n]['urgent'] == True]
for n in range(len(urg)):
    urg[n] = ws_nums[urg[n]] - 1

for n in range(10):
    if focused == n:
        workspace_string += '%{A:i3-msg workspace number ' + str(n+1) + ':}' + '%{F#FFFFFF}' + str(n+1) + '%{F-}%{A}' + ' '
    elif n in urg:
        workspace_string += '%{A:i3-msg workspace number ' + str(n+1) + ':}' + '%{F#FF0000}' + str(n+1) + '%{F-}%{A}' + ' '
    elif n+1 in ws_nums:
        workspace_string += '%{A:i3-msg workspace number ' + str(n+1) + ':}' + '%{F#999999}' + str(n+1) + '%{F-}%{A}' + ' '

if focused == 9:
        print('%{A4:i3-msg workspace prev:}',workspace_string,'%{A}',flush=True)
elif focused == 0:     
        print('%{A5:i3-msg workspace next:}',workspace_string,'%{A}',flush=True)
else:
        print('%{A4:i3-msg workspace prev:}%{A5:i3-msg workspace next:}',workspace_string,'%{A}%{A}',flush=True)

# Subscribe to events
i3.on('workspace', on_workspace_focus)

# Start the main loop and wait for events to come in
i3.main()

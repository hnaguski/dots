#!/bin/bash
# Script to get the battery level

# Grab current battery level
level=$(cat /sys/class/power_supply/BAT0/capacity)

# Figure out if the battery is plugged in
state=$(cat /sys/class/power_supply/ADP0/online)

# Okay making a lot of if statements to get the icon for battery level
if [[ level -ge 0 ]] && [[ level -lt 10 ]]; then
	{
		icon=""
	}
elif [[ level -ge 10 ]] && [[ level -lt 20 ]]; then
	{
		icon=""
	}
elif [[ level -ge 20 ]] && [[ level -lt 30 ]]; then
	{
		icon=""
	}
elif [[ level -ge 30 ]] && [[ level -lt 40 ]]; then
	{
		icon=""
	}
elif [[ level -ge 40 ]] && [[ level -lt 50 ]]; then
	{
		icon=""
	}
elif [[ level -ge 50 ]] && [[ level -lt 60 ]]; then
	{
		icon=""
	}
elif [[ level -ge 60 ]] && [[ level -lt 70 ]]; then
	{
		icon=""
	}
elif [[ level -ge 70 ]] && [[ level -lt 80 ]]; then
	{
		icon=""
	}
elif [[ level -ge 80 ]] && [[ level -lt 90 ]]; then
	{
		icon=""
	}
elif [[ level -ge 90 ]] && [[ level -le 100 ]]; then
	{
		icon=""
	}
fi

# Apply charging indicator () if the battery is charging and generate output
if [[ $state == 1 ]]; then
	{
		output="${icon}: $level% │ "
	}
else
	{
		output=" ${icon}: $level% │ "
	}
fi

#output
echo "$output"

#!/bin/bash
# Script to display wifi connection information

# First we need to figure out the name of the wifi interface
ifname=$(nmcli device | grep wifi | awk '{print $1}')

#Get the state of the wifi device
state=$(nmcli device | grep wifi | awk '{print $3}')

#if there is an active connection,
if [[ $state == 'connected' ]]; then
	{
		# Get signal strength
		signal=$(nmcli -f IN-USE,SIGNAL dev wifi | grep \* | awk '{print $2}')
		
		# Get ssid
		ssid=$(nmcli -t -f IN-USE,SSID device wifi | grep \* | cut -d ':' -f 2)

		# find out the ip address
		ip=$(nmcli -f IP4.ADDRESS device show $ifname | awk '{print $2}' | cut -d '/' -f 1)

		# generate the output with a separator
		output="直: ${signal}% at ${ssid} (${ip}) │ "
	}
# if there is no active wifi conenction
else
	{
		output=""
	}
fi

# give the output
echo "$output"

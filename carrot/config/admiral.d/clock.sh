#!/bin/bash

# Get the date/time
time=$(date '+%I:%M %p')

# Figure out what hour it is
hour=$(date '+%I')

# Case to set the icon based on what time it is
case $hour in
	01) icon="" ;;
	02) icon="" ;;
	03) icon="" ;;
	04) icon="" ;;
	05) icon="" ;;
	06) icon="" ;;
	07) icon="" ;;
	08) icon="" ;;
	09) icon="" ;;
	10) icon="" ;;
	11) icon="" ;;
	12) icon="" ;;
esac

# Generate and send the output
echo "${icon}: ${time} "

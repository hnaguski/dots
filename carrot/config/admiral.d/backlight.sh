#!/bin/bash
# Script to get the backlight value

# Grab backlight value and remove all the significant figures past the '.'
power=$(xbacklight -get | cut -d '.' -f 1)

# Generate and send the output
echo %{A4: xbacklight -inc 5 -steps 1:}%{A5:'xbacklight -dec 5 -steps 1':} ": $power% │ " %{A}%{A}

#!/bin/bash
# Script to get ethernet IP address

# get name of ethernet interface
ifname=$(nmcli device | grep ethernet | awk '{print $1}')

# Figure out if you have an active ethernet connection, should be either 'up' or 'down'
state=$(cat /sys/class/net/$ifname/operstate)

# If you have an active ethernet connection,
if [[ $state == 'up' ]]; then
	{
		# Find out the ip address
		ip=$(nmcli -f IP4.ADDRESS device show enp3s0 | awk '{print $2}' | cut -d '/' -f 1)
		# and generate the output
		output="囹: $ip │ "
	}
#If you don't
else
	{
		# Output nothing
		output=""
	}
fi

echo "$output"

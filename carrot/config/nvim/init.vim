set nocompatible              " be iMproved, required
filetype off                  " required for vundle

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim

" set where vundle installs plugins
call vundle#begin('~/.config/nvim/bundle/')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" ===============
" Plugins go here
" ===============

" YouCompleteMe completion engine
Plugin 'Valloric/YouCompleteMe'

" Vim Airline Statusbar
Plugin 'vim-airline/vim-airline'

" Wal colors for Vim
Plugin 'dylanaraps/wal.vim'

" Highlight color codes with their color
Plugin 'ap/vim-css-color'

" Tweaking html tags and surroundings easily (ie parentheses, brackets...)
Plugin 'tpope/vim-surround'

" Make number incrementing work with dates
Plugin 'tpope/vim-speeddating'

" ============================
" DON'T PUT PLUGINS BELOW THIS
" ============================

call vundle#end()            " required
filetype plugin indent on    " required

" Do :h vundle for more information/help with vundle

" ==============
" Default Config
" ==============

" Configure YCM to use correct version of python
let g:ycm_server_python_interpreter = '/usr/bin/python'

" Set the colorscheme to system colors (wal)
set notermguicolors
colorscheme wal

" Fix for airline to use correct symbols
let g:airline_powerline_fonts = 1

" Change tab size 
set tabstop=4 
set shiftwidth=4

" Turn on syntax highlighting (really useful)
syntax enable

" Multiline indenting
set breakindent

" Enable line numbers
set number

" Better command line completion
set wildmenu
set ignorecase
set smartcase

" Enable better mouse support
set mouse=a " enable the mouse in all modes

" Allow for more movement with arrows/hjkl
set whichwrap=b,s,<,>,[,]



